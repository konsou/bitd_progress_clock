<?php
include_once("site_settings.php");

$this_page = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);

if (isset($_GET['id'])){ 
    $session_id = $_GET['id']; 
}
else { 
    // If no session id set: generate new session id, redirect to page with id as GET parameter
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
        $url = "https://";   
    else  
        $url = "http://";   
    // Append the host(domain name, ip) to the URL.   
    $url.= $_SERVER['HTTP_HOST'];   
    
    // Append the requested resource location to the URL   
    $url.= $_SERVER['REQUEST_URI'];   
    $session_id = uniqid();

    header("Location: {$url}?id={$session_id}");
    exit();
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Shared Progress Clocks</title>
        <script src="jquery-3.4.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="scripts.js"></script>
        <script src="circle_segment.js"></script>
        <link rel="stylesheet" href="styles.css">
        <meta charset="UTF-8">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163209301-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-163209301-1');
        </script>
    </head>
    <body>
        <div id="link-to-this-page">
            <a href="<?php echo $this_page.'?id='.$session_id; ?>">Link to this page</a>
            - <a href="<?php echo $this_page;?>" target="_blank">New blank page</a>
        </div>
        <div id="add-clock-button-container">
            <h3>Add new clocks:</h3>
            <button id="button-add-clock-4" data-segments="4" class="button-add-clock">4 segments</button>
            <button id="button-add-clock-6" data-segments="6" class="button-add-clock">6 segments</button>
            <button id="button-add-clock-8" data-segments="8" class="button-add-clock">8 segments</button>
        </div>
        <input id="session_id" name="session_id" type="hidden" value="<?php echo $session_id; ?>" />
        <input id="title_max_length" name="title_max_length" type="hidden" value="<?php echo TITLE_MAX_LENGTH; ?>" />
    </body>
</html>