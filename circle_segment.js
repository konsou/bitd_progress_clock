const cos = Math.cos;
const sin = Math.sin;
const π = Math.PI;

const f_matrix_times = (( [[a,b], [c,d]], [x,y]) => [ a * x + b * y, c * x + d * y]);
const f_rotate_matrix = ((x) => [[cos(x),-sin(x)], [sin(x), cos(x)]]);
const f_vec_add = (([a1, a2], [b1, b2]) => [a1 + b1, a2 + b2]);

const segment = (([cx,cy],[rx,ry], [t1, Δ], φ ) => {
/* [
returns a SVG path element that represent a progress segment.
cx,cy → center of ellipse
rx,ry → major minor radius
t1 → start angle, in radian.
Δ → angle to sweep, in radian. positive.
φ → rotation on the whole, in radian
url: SVG Circle Arc http://xahlee.info/js/svg_circle_arc.html
Version 2019-06-19
 ] */
Δ = Δ % (2*π);
const rotMatrix = f_rotate_matrix (φ);
const [sX, sY] = ( f_vec_add ( f_matrix_times ( rotMatrix, [rx * cos(t1), ry * sin(t1)] ), [cx,cy] ) );
const [eX, eY] = ( f_vec_add ( f_matrix_times ( rotMatrix, [rx * cos(t1+Δ), ry * sin(t1+Δ)] ), [cx,cy] ) );
const fA = ( (  Δ > π ) ? 1 : 0 );
const fS = ( (  Δ > 0 ) ? 1 : 0 );
const path_2wk2r = document. createElementNS("http://www.w3.org/2000/svg", "path");
path_2wk2r.setAttribute("d", "M " + cx + " " + cy + " L " + sX + " " + sY + " A " + [ rx , ry , φ / (2*π) *360, fA, fS, eX, eY ].join(" ") 
                        + " L " + cx + " " + cy);
path_2wk2r.setAttribute("stroke", "black");
path_2wk2r.setAttribute("stroke-width", "4");
path_2wk2r.setAttribute("fill", "white");

return path_2wk2r;
});
