function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
  }



function createClockFrontend(segments, radius, padding, clock_id, text){
    // return div that contains svg clock
    const clock_svg = document. createElementNS("http://www.w3.org/2000/svg", "svg");
    const width = 2 * radius + 2 * padding;
    const height = 2 * radius + 2 * padding;
    const centerX = width / 2;
    const centerY = height / 2;

    console.log("In createClockFrontend - segments: "+ segments)
    
    var containerDiv = document.createElement("div");
    $(containerDiv).prop("id", `clock-container-${clock_id}`);
    $(containerDiv).addClass("clock-container");
    $(containerDiv).draggable({stack: "*"}); // Bring dragged item always to the front
    $(containerDiv).data("clock-id", clock_id);

    // TITLE CONTAINER DIV
    var titleContainer = document.createElement("div");
    $(titleContainer).addClass("clock-title-container");

    // CLOSE BUTTON
    var closeButton = document.createElement("img");
    $(closeButton).prop("src", "red_x.svg");
    $(closeButton).addClass("clock-close-button");
    $(closeButton).click(deleteClock);

    // TITLE TEXT
    var titleText = document.createElement("input");
    $(titleText).prop("id", `clock-title-${clock_id}`);
    $(titleText).prop("type", "text");
    $(titleText).prop("size", "5");
    $(titleText).prop("maxlength", $("#title_max_length").val());
    $(titleText).addClass("clock-title-text");
    $(titleText).val(text);
    $(titleText).data("user-edit-in-progress", "false");
    $(titleText).data("clock-id", clock_id);

    
    // Prevent auto updates from server if user is editing the text
    $(titleText).focus(function(){ 
        $(this).data("user-edit-in-progress", "true");
        $(this).data("original-value", $(this).val());
        // console.log(`${$(this).data("user-edit-in-progress")}`);
        });
    $(titleText).focusout(function(){ 
        $(this).data("user-edit-in-progress", "false")
        // console.log(`${$(this).data("user-edit-in-progress")}`);
        if ($(this).val() != $(this).data("original-value")){
            //sendClockData(clock_id);
            sendNewEvent($("html").data("session-id"), "update_title", clock_id, $(this).val());
        }
        });
    $(titleText).keyup(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        //console.log(keycode);
        if(keycode == '13'){ // ENTER
            $(this).blur();
            }
        else if (keycode == '27'){ // ESC
            $(this).val($(this).data("original-value"));
            $(this).blur();
        }
        });
    
    //$(titleText).change(function(){ console.log("changed")});

    

    // CLOCK SVG
    $(clock_svg).prop("id", `clock-svg-${clock_id}`);
    $(clock_svg).width(width);
    $(clock_svg).height(height);
    $(clock_svg).addClass("clock-svg");
    
    for (var i = 0; i < segments; i++){
        var sweep_angle = 2 * Math.PI / segments;
        var start_angle = i * sweep_angle;

        var seg = segment([centerX, centerY], [radius, radius], [start_angle, sweep_angle], 0);
        $(seg).click(toggleFillColor);
        $(clock_svg).append(seg);
    }

    $(clock_svg).css("transform", "rotate(-90deg)");

    // ASSEMBLING EVERYTHING TOGETHER
    $(titleContainer).append(titleText);
    $(titleContainer).append(closeButton);

    $(containerDiv).append(titleContainer);
    //$(containerDiv).append(titleText);
    $(containerDiv).append(clock_svg);
    // $(containerDiv).append(segmentNumberButtonContainer);

    return containerDiv;
}


function fetchClocksFull(session_id){
    console.log("Fetching clocks full state for session "+session_id);
    var request = $.ajax({
        url: "ajax_calls.php",
        type: "post",
        data: {
                "command": "get_clocks",
                "id": session_id,
            }
    });
    request.done(function (response, textStatus, jqXHR){
        // console.log(response);
        response_parsed = JSON.parse(response);
        // console.log(response_parsed);
        // console.log(response_parsed[0].query_timestamp);
        try {
            var lastQueryTimestamp = response_parsed[0].query_timestamp;
            $("html").data("last-event-query-timestamp", response_parsed[0].query_timestamp);
        } catch (e){ 
            // don't update last query timestamp 
        }
        
        // console.log($("html").data("last-event-query-timestamp"));

        var activeClockIDs = [];

        response_parsed.forEach(function(currentClock){
            // console.log("updating values for clock " + currentClock.id); // + $(`clock-container-${currentClock.id}`).length);
            if ($(`#clock-container-${currentClock.id}`).length){
                //console.log("Clock " + currentClock.id + "exists");
                var clockContainerElement = $(`div#clock-container-${currentClock.id}`);
                var titleText = $(`div#clock-container-${currentClock.id} > input.clock-title-text`);
                var fillColor;

                activeClockIDs.push(currentClock.id);
                //console.log(`clock ${currentClock.id} is active - adding to array`);
                //console.log(activeClockIDs);
            
                var index = 0;
                var segmentValuesBinary = parseInt(currentClock.segment_value_int).toString(2).split("").reverse();
                // console.log(`segment values int: ${currentClock.segment_value_int} - binary: ${segmentValuesBinary}`);
                $(`#clock-svg-${currentClock.id}`).children().each(function(){
                    // console.log(`iterating path elements - index ${index}`);
                    // console.log(`binary value: ${segmentValuesBinary[index]}`);
                    
                    if (segmentValuesBinary[index] == 1){
                        fillColor = "black";
                    } else {
                        fillColor = "white";
                    }
                    // console.log(`fill color is ${fillColor}`);

                    $(this).css("fill", fillColor);
                    index++;
                    
                });

                // Only update text if user is not editing it
                if (titleText.data("user-edit-in-progress") == "false"){
                    titleText.val(currentClock.text);
                }
                

            } else {
                //console.log("Clock " + currentClock.id + "doesn't exist - creating...");
                $("body").append(createClockFrontend(parseInt(currentClock.segments), 50, 5, currentClock.id, currentClock.text));
            }
        });
        
    });
}

function deleteClock(){
    // Delete a clock in frontend and send a deletion event to backend
    var clockContainer = $(this).parents(".clock-container");
    var clockID = clockContainer.data("clock-id");
    var sessionID = $("#session_id").val();

    sendNewEvent(sessionID, "delete", clockID);
    
    // TODO: Find out how to do this async
    clockContainer.remove();
}

function sendNewEvent(sessionID, eventType, clockID, eventData){
    var requestData = {
        "command": "add_event",
        "session_id": sessionID,
        "event_type": eventType,
        "clock_id": clockID,
        "event_data": eventData,        
    }

    console.log(`in sendNewEvent - event data to send:`);
    console.log(requestData);

    var request = $.ajax({
        url: "ajax_calls.php",
        type: "post",
        data: requestData,
    });
    request.done(function (response, textStatus, jqXHR){
        console.log(response);
    });

}

function sendClockData(/*string*/clockID){

    clockContainer = $(`#clock-container-${clockID}`);

    clockSVG = $(`#clock-svg-${clockID}`);

    var segments = 0;
    var segmentvaluesBinary = "";
    var currentSegment = "";

    clockSVG.children().each(function(){
        segments++;
        currentPath = $(this);
        //console.log(currentPath);
        currentSegmentValue = (currentPath.css("fill") == "rgb(0, 0, 0)" ? "1" : "0");
        segmentvaluesBinary = `${currentSegmentValue}${segmentvaluesBinary}`;
    });

    var segmentValueInt = parseInt(segmentvaluesBinary, 2);

    var text = $(`#clock-title-${clockID}`).val();

    updateData = {
        "command": "update_clock",
        "clock_id": clockID,
        "segments": segments,
        "segment_values_binary": segmentvaluesBinary,
        "segment_value_int": segmentValueInt,
        "text": text,
    }

    console.log(updateData);

    var request = $.ajax({
        url: "ajax_calls.php",
        type: "post",
        data: updateData,
    });
    request.done(function (response, textStatus, jqXHR){
        console.log(response);
    });

}

function createClockBackend(segments){
    var sessionID = $("#session_id").val();
    var data = {
        "command": "create_clock",
        "id": sessionID,
        "segments": segments,
    };

    console.log("in createClock - data is ");
    console.log(data);

    var request = $.ajax({
        url: "ajax_calls.php",
        type: "post",
        data: data,
    });
    request.done(function (response, textStatus, jqXHR){
        console.log(response);
        fetchClocksFull(sessionID);
    });

}

function toggleFillColor(){
    $this = $(this);
    var clockID = parseInt($this.parent().prop("id").split("clock-svg-")[1]);
    //console.log(clock_id);

    if ($this.css("fill") == "rgb(0, 0, 0)"){
        $this.css({ fill: "white" });
    } else {
        $this.css({ fill: "black" });
    }
    // console.log($(this).css("fill"));

    sendClockData(clockID);
}

function fetchLatestEvents(){
    const sessionID = $("html").data("session-id");
    const lastEventQueryTimestamp = $("html").data("last-event-query-timestamp")

    const data = {
        "command": "get_events",
        "session_id": sessionID,
        "event_after_timestamp": lastEventQueryTimestamp,
    };

    console.log("in fetchLatestEvents - data is:");
    //console.log(data);

    var request = $.ajax({
        url: "ajax_calls.php",
        type: "post",
        data: data,
    });
    request.done(function (response, textStatus, jqXHR){
        console.log(response);
        var parsedResponse = JSON.parse(response);
        var queryTimeStamp;

        $("html").data("event-query-counter", $("html").data("event-query-counter") + 1);
        // console.log($("html").data("event-query-counter"));
        
        try {
            queryTimeStamp = parsedResponse[0].query_timestamp;
            $("html").data("last-event-query-timestamp", queryTimeStamp);

            handleEvents(parsedResponse);
        } catch(e) {
            /* don't update query timestamp */
        }

        //console.log();
        

    });

}

function handleEvents(eventList){
    eventList.forEach(function(currentEvent){
        console.log(`handling event ${currentEvent.id} (${currentEvent.type})`);
        //console.log(currentEvent);

        var clockID = currentEvent.clock_id;

        if (currentEvent.type == "delete"){
            if (clockID){ $(`#clock-container-${clockID}`).remove(); }
        }
        else if (currentEvent.type == "update_title"){
            if (clockID){
                var titleText = $(`#clock-title-${clockID}`);
                //console.log("locked: "+$(titleText).data("user-edit-in-progress"));
                if ($(titleText).data("user-edit-in-progress") == "false"){
                    // TODO: THIS *WILL* CAUSE SYNC PROBLEMS IN THE FUTURE! FIX 
                    //console.log("trying to update title")
                    $(titleText).val(currentEvent.data); 
                }

            }
        }
    });
}

$( document ).ready(function() {
    console.log("Ready")
    const session_id = $("#session_id").val();
    console.log(session_id);

    $("html").data("last-event-query-timestamp", "");
    $("html").data("session-id", session_id);
    $("html").data("event-query-counter", 0); // How many event fetches since last full state fetch

    fetchClocksFull(session_id);

    $(".button-add-clock").click(function(){
        var segments = parseInt($(this).data("segments"));
        createClockBackend(segments);
    });

    window.setInterval(function(){
        /*
        if ($("html").data("event-query-counter") <= 30){
            fetchLatestEvents(); 
        } else {
        */
        fetchClocksFull(session_id);
        $("html").data("event-query-counter", 0);
        //}
        
	}, 1000);
});

