<?php
include_once("db_settings.php");
include_once("site_settings.php");

function print_database_error_message(){
    echo "Database connection failed. Please notify site admin: <a href='mailto:".ADMIN_EMAIL."'>".ADMIN_EMAIL."</a>";
    die();
}

function connect_db(){
    // CONNECT TO DB
    try {
        $conn = new PDO("mysql:host=".DB_ADDRESS.";dbname=".DB_NAME, DB_USERNAME, DB_PASSWORD);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    catch(PDOException $e){
        print_database_error_message();
        }
    
        return $conn;
}

function get_clocks_for_id($conn, $session_id){
    try {
        $stmt = $conn->prepare("SELECT id, segments, segment_value_int, text, CURRENT_TIMESTAMP(3) AS 'query_timestamp' FROM clocks WHERE session_id = ?");
        $stmt->execute([$session_id]);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();

        return $result;
    }
    catch(PDOException $e) {
        print_database_error_message();
        }
}

function create_clock($conn, $session_id, $segments, $text){
    $text = substr($text, 0, TITLE_MAX_LENGTH);

    try {
        $sql = "INSERT INTO clocks (session_id, segments, text) VALUES (?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$session_id, $segments, $text]);
        }
    catch(PDOException $e){
        print_database_error_message();
        }
}

function update_clock($conn, int $clock_id, int $segments, int $segment_value_int, string $text){
    $text = substr($text, 0, TITLE_MAX_LENGTH);
    
    // segment values as packed int
    try {
        $sql = "UPDATE clocks SET segments=?, segment_value_int=?, text=? WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$segments, $segment_value_int, $text, $clock_id]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message(); }
        }

}

function update_clock_title($conn, int $clock_id, string $text){
    $text = substr($text, 0, TITLE_MAX_LENGTH);
    
    try {
        $sql = "UPDATE clocks SET text=? WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$text, $clock_id]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message(); }
        }
}

function update_clock_segments($conn, int $clock_id, int $segment_value_int){
    try {
        $sql = "UPDATE clocks SET segment_value_int=? WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$segment_value_int, $clock_id]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message(); }
        }
}


function delete_clock($conn, int $clock_id){
    try {
        $sql = "DELETE FROM clocks WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$clock_id]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message(); }
        }

}

function handle_event($conn, string $session_id, int $clock_id = 0, string $event_type, string $event_data = ""){
    if (in_array($event_type, array("delete", "update_title", "update_segments")) && $clock_id == 0){
        echo json_encode(array("error" => "handle_event for {$event_type} needs clock_id (was {$clock_id})")); 
        return;
    }
    if ($event_type == "delete"){
        delete_clock($conn, $clock_id);
    }
    else if ($event_type == "update_title"){
        update_clock_title($conn, $clock_id, $event_data);
    }
    else if ($event_type == "update_segments"){
        update_clock_segments($conn, $clock_id, intval($event_data));
    }
}

function add_event($conn, string $session_id, int $clock_id = 0, string $event_type, string $event_data = ""){
    echo "in add_event - session_id: {$session_id}, event_data:";
    
    try {
        $sql = "INSERT INTO events (session_id, clock_id, type, data) VALUES (?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$session_id, $clock_id, $event_type, $event_data]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message(); }
        }
}

function get_events($conn, string $session_id, string $after_timestamp = ""){
    // timestamp in MySQL format: 2020-04-03 16:03:51.473

    try {
        $stmt = $conn->prepare("SELECT *, CURRENT_TIMESTAMP(3) AS 'query_timestamp' FROM events WHERE session_id = ? AND timestamp > ? ORDER BY timestamp ASC");
        $stmt->execute([$session_id, $after_timestamp]);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();

        return $result;
    }
    catch(PDOException $e) {
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message(); }
        }

}
?>