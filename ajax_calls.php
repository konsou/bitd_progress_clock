<?php
include_once("db_settings.php");
include_once("site_settings.php");
include_once("functions.php");

$session_id = $_POST['id'] ?? ($_POST['session_id'] ?? 0);
$clock_id = $_POST['clock_id'] ?? 0;
$command = $_POST['command'] ?? "";
$segments = $_POST['segments'] ?? 4;
$segment_value_int = $_POST['segment_value_int'] ?? 0;
$text = $_POST['text'] ?? "TITLE";
$event_data = $_POST['event_data'] ?? "";
$event_type = $_POST['event_type'] ?? "";
$event_after_timestamp = $_POST['event_after_timestamp'] ?? "";


// ERROR HANDLING
if ($command == ""){ echo json_encode(array("error" => "command not set")); die(); }

else if (in_array($command, array("get_clocks", "create_clock", "add_event", "get_events"))){
    if ($session_id == 0){ echo json_encode(array("error" => "session id not set")); die(); }
}

if ($command == "update_clock" || $command == "delete_clock"){
    if ($clock_id == 0){ echo json_encode(array("error" => "clock id not set")); die();  }
}

if ($command == "add_event" && !$event_type) {
    echo json_encode(array("error" => "empty event type")); die();
}


$conn = connect_db();

if ($command == "get_clocks"){
    $active_clocks = get_clocks_for_id($conn, $session_id);
    echo json_encode($active_clocks);
}
else if ($command == "create_clock"){
    create_clock($conn, $session_id, $segments, $text);
}
else if ($command == "update_clock"){
    update_clock($conn, $clock_id, $segments, $segment_value_int, $text);
}
else if ($command == "delete_clock"){
    delete_clock($conn, $clock_id);
}
else if ($command == "add_event"){
    // Add event to be sent to other clients
    add_event($conn, $session_id, $clock_id, $event_type, $event_data);
    // Handle event if it needs to change DB content
    handle_event($conn, $session_id, $clock_id, $event_type, $event_data);
}
else if ($command == "get_events"){
    echo json_encode(get_events($conn, $session_id, $event_after_timestamp));
}

$conn = null;    
?>